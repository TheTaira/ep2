# Exercício Programado 2

Pokedex para cadastro de treinadores e armazenamento dos pokemon de cada treindador.

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps/eps_2018_2/ep2/wikis/home).

## Selecao de treinadores

O programa are na tela de selecao de treinadores, que comeca vazia.

![Screenshot_20181106-234251_Pokedex](/uploads/530c0b31bcb31163105b3a2a786a2593/Screenshot_20181106-234251_Pokedex.jpg)

Para adicionar um novo treinador o usuario deve tocar no botao "NOVO TREINADOR".
O aplicativo entao abre uma tela onde o usuario pode escrever o nome em questao. Apos escrito o nome o usuario deve tocar no botao "ADICIONAR" para que seja guardado o novo treinador.

![Screenshot_20181106-234256_Pokedex](/uploads/0e63e38b069a9313f3931710323d7fba/Screenshot_20181106-234256_Pokedex.jpg)

![Screenshot_20181107-002041_Pokedex](/uploads/c155a35f983a0f2b155170ab28eaf04b/Screenshot_20181107-002041_Pokedex.jpg)

Apos ser adicionado um novo treinador o programa retorna a selecao de treinadores.

![Screenshot_20181106-234913_Pokedex](/uploads/58898647ce88af99477268e0ca080f5f/Screenshot_20181106-234913_Pokedex.jpg)

Para selecionar um treinador o usuario deve tocar no nome do treindador desejado

## Tela do treinador

Apos selecionado um treinador e mostrada a tela do treinador, onde sao mostrados os pokemon que este possui.

![Screenshot_20181106-235744_Pokedex](/uploads/e15632a183698e495d9573429eb1be4a/Screenshot_20181106-235744_Pokedex.jpg)

Para adicionar pokemon ao treinador basta clicar no botao "ADICIONAR POKEMON"

![Screenshot_20181107-002051_Pokedex](/uploads/110f5f76165f8eb5e89f570af944e2ca/Screenshot_20181107-002051_Pokedex.jpg)

E mostrada entao uma tela que permite ver todos os pokemon disponiveis assim como pesquisar por nome por um pokemon especifico.

![Screenshot_20181107-000712_Pokedex](/uploads/47ef3f12cd991311bf482cae4cd98d1e/Screenshot_20181107-000712_Pokedex.jpg)

Ao tocar uma vez em cima de qualquer pokemon sao mostrados suas estatisticas.

![Screenshot_20181107-000729_Pokedex](/uploads/f4cc1f9d049e6507eda97584a0feca87/Screenshot_20181107-000729_Pokedex.jpg)

Utilizando o botao de voltar do android o aplicativo retorna para a tela de escolha de pokemon.
para adicionar um pokemon ao treinador basta que o usuario encoste no nome do pokemon desejado ate que apareca uma mensagem dizendo que o pokemon foi adicionado.

![Screenshot_20181107-000734_Pokedex](/uploads/fd574e4508e8ee896b835f9e906193ce/Screenshot_20181107-000734_Pokedex.jpg)


Podem ser adicionados quantos pokemon forem desejados.

Voltando para a tela anterior usando o botao de voltar do android o aplicativo retorna para tela do treinador.
Para que sejam mostrados os pokemon recem adicionados e nescessario que o usuario toque o botao "RECARREGAR".

![Screenshot_20181107-000916_Pokedex](/uploads/b5b7aa428cd3f4900780e4a9590e65c5/Screenshot_20181107-000916_Pokedex.jpg)

Na tela do treinador e possivel tambem tocar uma vez no pokemon para ver suas estatisticas.
E possivel remover pokemon usando o botao "REMOVER POKEMON" que levara o usuario a uma tela em que sao mostrados somente os pokemon que este possui.

![Screenshot_20181107-001149_Pokedex](/uploads/f5e4ee2036d0c39cc35e7abff96fa3ce/Screenshot_20181107-001149_Pokedex.jpg)

nesta tela e possivel tocar uma vez para ver as estatisticas do pokemon e tocar e segurar para remover o pokemon.

Podem ser criados quantos treinadores se desejar e cada um pode ter quantos pokemon se desejar.
