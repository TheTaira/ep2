package com.example.taira.pokedex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class StatsShow extends AppCompatActivity {
    private static final String LOLSTAT = "to_lol_stat";
    private static final String TAG = "EITA";
    private Pokemon pokemon = new Pokemon();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats_show);
        Intent intet = getIntent();
        try{
            pokemon.setNumber(Integer.valueOf(intet.getStringExtra(LOLSTAT)));
        }catch (Exception e){
            Log.d(TAG, "onCreate: fucjk: " + e);
        }
        Log.d(TAG, "onCreate: namer: " + pokemon.getNumber());
        try {
            requestans();
        }catch (Exception e){
            Log.d(TAG, "onCreate: fodeo: " + e);
        }
    }

    protected void requestans() throws IOException, JSONException {
        TheRequest request = new TheRequest();

        String text = request.makeTheRequest("https://pokeapi.co/api/v2/pokemon/");

        JSONObject json = new JSONObject(text);
        JSONArray array = json.getJSONArray("results");

        String name;
        String type;
        String url;
        String textUnico;

        name = array.getJSONObject(pokemon.getNumber()).get("name").toString();


        if(pokemon.getNumber()>=802){
            url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + (pokemon.getNumber()+9199) + ".png";
        }else{
            url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + (pokemon.getNumber()+1) + ".png";
        }
        pokemon.setName(name);
        pokemon.setAvatarLocation(url);

        if(pokemon.getNumber() >=802){
            text = request.makeTheRequest("https://pokeapi.co/api/v2/pokemon/" + (pokemon.getNumber()+9199) + "/");
        }else{
            text = request.makeTheRequest("https://pokeapi.co/api/v2/pokemon/" + (pokemon.getNumber()+1) + "/");
        }
        //text = request.makeTheRequest("https://pokeapi.co/api/v2/pokemon/" + (pokemon.getNumber()+1) + "/");
        json = new JSONObject(text);
        array = json.getJSONArray("stats");

        Log.d(TAG, "requestans: array: " + array);


        makingStats(array);
    }
    public void makingStats(JSONArray array) throws JSONException {

        ImageView imageView = findViewById(R.id.avatarView);
        TextView pokenome = findViewById(R.id.textViewStatNome);
        TextView textStat0 = findViewById(R.id.textViewStat0);
        TextView textStat1 = findViewById(R.id.textViewStat1);
        TextView textStat2 = findViewById(R.id.textViewStat2);
        TextView textStat3 = findViewById(R.id.textViewStat3);
        TextView textStat4 = findViewById(R.id.textViewStat4);
        TextView textStat5 = findViewById(R.id.textViewStat5);

        ArrayList<TextView> textos = new ArrayList<>();
        textos.add(textStat0);
        textos.add(textStat1);
        textos.add(textStat2);
        textos.add(textStat3);
        textos.add(textStat4);
        textos.add(textStat5);

        Glide.with(this)
                .asBitmap()
                .load(pokemon.getAvatarLocation())
                .into(imageView);
        pokenome.setText(pokemon.getName());

        JSONObject obj1;
        JSONObject obj2;
        String stat;
        Integer num;

        for (int j=0; j<6; j++) {
            obj1 = (JSONObject) array.getJSONObject(j);
            obj2 = obj1.getJSONObject("stat");
            num = obj1.getInt("base_stat");
            stat = obj2.getString("name");
            textos.get(j).setText(stat + ": " + num);
        }

    }

}
