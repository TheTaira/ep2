package com.example.taira.pokedex;

import com.google.gson.annotations.Expose;

public class Pokemon {
    @Expose
    private String name;
    private int number;
    private  String avatarLocation;
    private String type;

    public Pokemon (String name, int number, String avatarLocation){
        setName(name);
        setNumber(number);
        setAvatarLocation(avatarLocation);
    }
    public Pokemon (String name, int number, String avatarLocation, String type){
        setName(name);
        setNumber(number);
        setAvatarLocation(avatarLocation);
        setType(type);

    }
    public Pokemon(){};

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getAvatarLocation() {
        return avatarLocation;
    }

    public void setAvatarLocation(String avatarLocation) {
        this.avatarLocation = avatarLocation;
    }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

};
