package com.example.taira.pokedex;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Treinador implements Parcelable {
    private String name;
    private ArrayList<Pokemon> pokemonArray;


    protected Treinador(Parcel in) {
        name = in.readString();
}

    public static final Creator<Treinador> CREATOR = new Creator<Treinador>() {
        @Override
        public Treinador createFromParcel(Parcel in) {
            return new Treinador(in);
        }

        @Override
        public Treinador[] newArray(int size) {
            return new Treinador[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Pokemon> getPokemonArray() {
        return pokemonArray;
    }

    public void setPokemonArray(ArrayList<Pokemon> pokemonArray) {
        this.pokemonArray = pokemonArray;
    }
    public Treinador(String name){
        this.name = name;
    }
    public Treinador(){};

    public Treinador(String name, ArrayList<Pokemon> pokemonArray){
        this.name = name;
        this.pokemonArray = pokemonArray;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
    }
}
