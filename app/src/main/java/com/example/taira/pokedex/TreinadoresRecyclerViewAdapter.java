package com.example.taira.pokedex;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;




public class TreinadoresRecyclerViewAdapter extends RecyclerView.Adapter<TreinadoresRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "EITA";
    public static final String LOLZAOSTRING = "lol_string";
    public static final String LOLZAOINTARRAY = "lol_int_array";

    private Context context;
    private ArrayList<Treinador> mTreinadores;
    private Treinador treinadorAtual = new Treinador();




    public TreinadoresRecyclerViewAdapter(Context mContext, ArrayList <Treinador> mTreinadores){

        this.context = mContext;
        this.mTreinadores = mTreinadores;
    }

    @NonNull
    @Override

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_treinador, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        for (int j=0; j<mTreinadores.size(); j++) {
            Log.d(TAG, "onCreateViewHolder: trein: " + mTreinadores.get(j).getName());
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {

        viewHolder.textView.setText(mTreinadores.get(position).getName());

        Log.d(TAG, "onBindViewHolder: trainer pokenom " + mTreinadores.get(position).getName());

        viewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent goToTreinador = new Intent(context, TelaTreinador.class);

                goToTreinador.putExtra(LOLZAOSTRING, mTreinadores.get(viewHolder.getAdapterPosition()).getName());

                context.startActivity(goToTreinador);
                Log.d(TAG, "onClick: depois");
            }
        });

    }



    @Override
    public int getItemCount() {
        return mTreinadores.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        RelativeLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.textView4);
            relativeLayout = itemView.findViewById(R.id.item_treinador);
        }
    }

    public Treinador getTreinadorAtual() {
        return treinadorAtual;
    }

    public void setTreinadorAtual(Treinador treinadorAtual) {
        this.treinadorAtual = treinadorAtual;
    }


    public void writeOnFile () throws IOException {

        String filePath = context.getFilesDir().getPath().toString() + "/fileName.txt";
        Log.d(TAG, "writeOnFile: file: " + filePath);

        String str = "1,2,3,";
        Log.d(TAG, "writeOnFile: writing");
        BufferedWriter writer = new BufferedWriter(new FileWriter("/data/user/0/com.example.taira.pokedex/files/texto.csv"));
        writer.append(str);

        writer.close();

    }
}
