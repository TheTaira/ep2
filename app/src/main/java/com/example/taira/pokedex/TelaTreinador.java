package com.example.taira.pokedex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class TelaTreinador extends AppCompatActivity {

    public static final String LOLZAOSTRING = "lol_string";
    public static final String LOLZAOSTRINGADD = "lol_string_main";
    public static final String LOLZAOSTRINGREMOVE = "lol_string_remove";
    private static final String TAG = "EITA";
    private Treinador oTreinador = new Treinador();
    private TheRequest request = new TheRequest();
    private ArrayList <Pokemon> mPokemon = new ArrayList<>();
    private RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, mPokemon, "nothing");
    private ArrayList<Integer> intArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_treinador);

        final TextView nomeTreinador = findViewById(R.id.textViewNomeTreinador);
        final Intent intent = getIntent();
        final String trainerName = intent.getStringExtra(LOLZAOSTRING);
        oTreinador.setName(trainerName);
        nomeTreinador.setText(trainerName);
        Log.d(TAG, "onCreate: log this, bitch " + trainerName);
        Button botaoAdicionar = findViewById(R.id.botao_adicionar);
        Button botaoRemover = findViewById(R.id.botao_remover);
        Button botaoRecarregar = findViewById(R.id.button_recarregar);
        adapter.notifyDataSetChanged();
        try { initPokemonTreinador();
        } catch (Exception e) { Log.d(TAG, "onCreate: pokes exep: " + e );
        }

        botaoAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: intentzao add");
                Intent intentToAdd = new Intent(TelaTreinador.this, MainActivity.class);
                Log.d(TAG, "onClick: aftent");

                intentToAdd.putExtra(LOLZAOSTRINGADD, trainerName);
                Log.d(TAG, "onClick: aftExtra");
                startActivity(intentToAdd);
            }
        });
        botaoRemover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: intend remove");
                Intent intentToRemove = new Intent(TelaTreinador.this, EscolhaRemover.class);
                Log.d(TAG, "onClick: traninae: " + trainerName);
                intentToRemove.putExtra(LOLZAOSTRINGREMOVE, trainerName);

                startActivity(intentToRemove);
            }
        }

        );
        botaoRecarregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent thentent =new Intent(TelaTreinador.this, TelaTreinador.class);
//                adapter.notifyDataSetChanged();
                thentent.putExtra(LOLZAOSTRING, trainerName);

                startActivity(thentent);
            }
        });


    }

    protected void initPokemonTreinador() throws JSONException, IOException {

    String text = request.makeTheRequest("https://pokeapi.co/api/v2/pokemon/");
    JSONObject json = new JSONObject(text);
    JSONArray array = json.getJSONArray("results");
    String name;
    String url;
    Integer realI;
    intArray = readFromFile();
    initRecyclerView();

        for (int i=0 ; i<intArray.size() ; i++) {
        realI = intArray.get(i);
        Log.d(TAG, "initPokemonTreinador: realI: " + realI);
        name = array.getJSONObject( realI ).get("name").toString();

        if(realI>=802){ url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + ( realI + 9199) + ".png";
//            if(realI>=802){ url = "https://pps.whatsapp.net/v/t61.11540-24/31097933_1054503728036861_2533668711911391232_n.jpg?oe=5BE5A193&oh=1ed8e5173ea2981d6b6f1411b86d9050";
            }else{ url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + (realI +1) + ".png";
        }
        mPokemon.add(new Pokemon(name, realI, url));
        adapter.notifyItemInserted(i);
        }
        oTreinador.setPokemonArray(mPokemon);
    }

    private void initRecyclerView(){

        Log.d(TAG, "initRecyclerView: init RecyclerView");

        RecyclerView recyclerView = findViewById(R.id.RecyclerViewPokemonTreinador);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public  ArrayList <Integer> readFromFile() throws IOException {
        Log.d(TAG, "readFromFile: ithsi is  tghe paathh: " + getDataDir());
        String path = getDataDir() + "/files/" + oTreinador.getName() + ".csv";
        BufferedReader reader = new BufferedReader(new FileReader(path));
        String[] fields = new String[0];
        ArrayList <String> list = new ArrayList<>();
        ArrayList<Integer> intList = new ArrayList<>();
        Log.d(TAG, "readFromFile: " + path);

       while (true) {
           String line = reader.readLine();
           Log.d(TAG, "readFromFile: onTreinador: " + line);
           if (line == null)
               break;
           fields = line.split(",");
           for (int i=0; i<fields.length; i++){
               Log.d(TAG, "readFromFile: fields: " + fields[i]);
           }
       }
       for (int i=0; i< fields.length-1; i++) {

           Log.d(TAG, "readFromFile: adding: " + fields[i+1]);
           list.add(fields[i+1]);

           intList.add(Integer.valueOf(list.get(i)));
           Log.d(TAG, "readFromFile: lol: " + intList.get(i));
       }
       reader.close();
       return intList;
    }



}
