package com.example.taira.pokedex;

import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class TheRequest {
    private static final String TAG = "EITA";
    private String url;
    public TheRequest() {
        this.url = "";
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public String makeTheRequest(String url) throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        this.url = url;

        URL urlObj = new URL(url);

        HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();

        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestMethod("GET");

        Log.d(TAG, "makeTheRequest: opa");
            try{

                int responseCode = con.getResponseCode();
                Log.d(TAG, "makeTheRequest: "+responseCode);
            }

            catch (Exception e) {
                Log.d(TAG, "makeTheRequest: catch");
            }

        String message = con.getResponseMessage();

        String inputLine;

        StringBuffer buffer = new StringBuffer();





        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));

        while ((inputLine = in.readLine()) != null) {
            buffer.append(inputLine);
        }
        in.close();
        con.disconnect();


        return buffer.toString();

    }
}



