package com.example.taira.pokedex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class MakeNewTreinador extends AppCompatActivity {
    private static final String TAG = "EITA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_new_treinador);
        Button botao = findViewById(R.id.botaoConfirmarNovoTreinador);

        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    adicionaNovoTreinador();

                } catch (IOException e) {
                    try {

                        File file1 = new File(getDataDir() + "/files/");
                        File file2 = new File(getDataDir() + "/files/treinadores.csv");
                        file1.mkdir();
                        file2.createNewFile();

                        adicionaNovoTreinador();
                    } catch (IOException e1) {
                        Log.d(TAG, "onClick: fjuckmylife");
                    }
                    Log.d(TAG, "onClick: botao de adicionar: " + e);
                }
            }
        });

    }

    public void adicionaNovoTreinador() throws IOException {

        Log.d(TAG, "adicionaNovoTreinador: lel");
        EditText editText = findViewById(R.id.editTextNovoTreinador);
        String nomeNovo = String.valueOf(editText.getText());

        //String pokeStorePath =  getDataDir() + "/files/treinadores.csv" + trainerName + ".csv";
//        BufferedWriter writer = new BufferedWriter(new FileWriter(getDataDir() + "/files/treinadores.csv"));
//        writer.close();
        Log.d(TAG, "adicionaNovoTreinador: eloaesw");
        BufferedReader reader = new BufferedReader(new FileReader(getDataDir() + "/files/treinadores.csv"));
        Log.d(TAG, "adicionaNovoTreinador: lasdasd");

        String line = reader.readLine();

        Log.d(TAG, "adicionaNovoTreinador: linha1: " + line);
        reader.close();

        BufferedWriter writer = new BufferedWriter(new FileWriter(getDataDir() + "/files/treinadores.csv"));

        if(line==null){
            writer.append(nomeNovo);
        }else {
            writer.append(line + "," + nomeNovo);
        }
        writer.close();
        BufferedWriter writer2 = new BufferedWriter(new FileWriter(getDataDir() + "/files/" + nomeNovo + ".csv"));
        writer2.close();

        reader = new BufferedReader(new FileReader(getDataDir() + "/files/treinadores.csv"));

        line =reader.readLine();

        Log.d(TAG, "adicionaNovoTreinador: linha2: " + line);

        startActivity(new Intent(this, EscolherTreinador.class));
    }
}
