package com.example.taira.pokedex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class EscolhaRemover extends AppCompatActivity {
    public static final String LOLZAOSTRINGREMOVE = "lol_string_remove";
    private static final String TAG = "EITA";
    private TheRequest request = new TheRequest();
    private Treinador oTreinador;
    private ArrayList<Integer> intArray;
    private ArrayList<Pokemon> mPokemon = new ArrayList<>();
    private RecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escolha_remover);

        Intent intentQueTrouxe = getIntent();
        String name = intentQueTrouxe.getStringExtra(LOLZAOSTRINGREMOVE);
        Log.d(TAG, "onCreate: name  " + name);
        oTreinador = new Treinador(name);
        adapter = new RecyclerViewAdapter(this, mPokemon, "remove", name);
        Log.d(TAG, "onCreate: trainame: " + oTreinador.getName());
        try{
            initPokemonTreinador();
        }catch (Exception e){
            Log.d(TAG, "onCreate: exep initpole: " + e);
        }
    }

    protected void initPokemonTreinador() throws JSONException, IOException {

        String text = request.makeTheRequest("https://pokeapi.co/api/v2/pokemon/");
        JSONObject json = new JSONObject(text);
        JSONArray array = json.getJSONArray("results");
        String name;
        String url;
        Integer realI;

        intArray = readFromFile();

        initRecyclerView();

        for (int i=0 ; i<intArray.size() ; i++) {
            realI = intArray.get(i);
            Log.d(TAG, "initPokemonTreinador: realI: " + realI);
            name = array.getJSONObject( realI ).get("name").toString();

            if(i>=802){ url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + ( realI +9199) + ".png";
            }else{ url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + (realI +1) + ".png";
            }
            mPokemon.add(new Pokemon(name, realI, url));
            adapter.notifyItemInserted(i);
        }
        oTreinador.setPokemonArray(mPokemon);
    }

    private void initRecyclerView(){

        Log.d(TAG, "initRecyclerView: init RecyclerView");

        RecyclerView recyclerView = findViewById(R.id.recyclerRemover);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public ArrayList<Integer> readFromFile() throws IOException {
        String path = getDataDir() + "/files/" + oTreinador.getName() + ".csv";
        BufferedReader reader = new BufferedReader(new FileReader(path));
        String[] fields = new String[0];
        ArrayList <String> list = new ArrayList<>();
        ArrayList<Integer> intList = new ArrayList<>();
        Log.d(TAG, "readFromFile: " + path);

        while (true) {
            String line = reader.readLine();
            Log.d(TAG, "readFromFile: onTreinador: " + line);
            if (line == null)
                break;
            fields = line.split(",");
            for (int i=0; i<fields.length; i++){
                Log.d(TAG, "readFromFile: fields: " + fields[i]);
            }
        }
        for (int i=0; i< fields.length-1; i++) {

            Log.d(TAG, "readFromFile: adding: " + fields[i+1]);
            list.add(fields[i+1]);

            intList.add(Integer.valueOf(list.get(i)));
            Log.d(TAG, "readFromFile: lol: " + intList.get(i));
        }
        reader.close();
        return intList;
    }
}
