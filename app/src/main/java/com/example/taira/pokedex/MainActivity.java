package com.example.taira.pokedex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;


import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = "EITA";
    public static final String LOLZAOSTRINGMAIN = "lol_string_main";
    private ArrayList<Pokemon> mPokemon = new ArrayList<>();
    private RecyclerViewAdapter adapter =  new RecyclerViewAdapter(this, mPokemon, "add");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: Started");

        try {
            initImageBitmaps();
        } catch (IOException e) {
            Log.d(TAG, "onCreate: IOExeption");
        } catch (JSONException e) {
            Log.d(TAG, "onCreate: JSONExeption");
        }
    }

    private void initImageBitmaps() throws IOException, JSONException {

        Integer n=1;
        TheRequest request = new TheRequest();

        Log.d(TAG, "initImageBitmaps: preparing bitmaps");
        String text ="lol";

        text = request.makeTheRequest("https://pokeapi.co/api/v2/pokemon/");

        JSONObject json = new JSONObject(text);
        JSONArray array = json.getJSONArray("results");

        String name;
        String type;
        String url;
        String textUnico;

        initRecyclerView();

        EditText pesquisa = findViewById(R.id.editTextPesquisa);

        pesquisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d(TAG, "afterTextChanged: shit: " + s.toString());
                filtrar(s.toString());
            }
        });

        for (int i=0 ; i<array.length() ; i++) {

            name = array.getJSONObject(i).get("name").toString();


            if(i>=802){
                url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + (i+9199) + ".png";
            }else{
                url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + (i+1) + ".png";
            }
            mPokemon.add(new Pokemon(name, i, url));
            adapter.notifyItemInserted(i);
        }
    }

    private void initRecyclerView(){

        Log.d(TAG, "initRecyclerView: init RecyclerView");

        RecyclerView recyclerView = findViewById(R.id.theRecyclerView);

        recyclerView.setAdapter(adapter);
        Intent intentQueTrouxe = getIntent();
        String treinadorNome = intentQueTrouxe.getStringExtra(LOLZAOSTRINGMAIN);

        adapter.setTrainerName(treinadorNome);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));



//        RecyclerViewAdapter adapter =  new RecyclerViewAdapter(this, mPokemon);

    }

    private void filtrar(String s){
        ArrayList<Pokemon> filteredPokemon = new ArrayList<>();

        for (Pokemon item: mPokemon){
            if(item.getName().toLowerCase().contains(s.toLowerCase())){
                filteredPokemon.add(item);
            }
        }
        adapter.filtraLista(filteredPokemon);
    }





}
