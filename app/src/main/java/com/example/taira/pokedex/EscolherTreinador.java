package com.example.taira.pokedex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class EscolherTreinador extends AppCompatActivity {
    private static final String TAG = "EITA";
    private ArrayList<Treinador> mTreinadores = new ArrayList<>();

    private Pokemon pokeAux = new Pokemon();
    private ArrayList<Pokemon> pokeArrayAux = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escolher_treinador);
        Log.d(TAG, "onCreate: owwk");
        initTreinadores();
    }

    protected void initTreinadores() {
        try {
            readFile();
        } catch (Exception e) {
            Log.d(TAG, "initTreinadores: e na fake " + e);
        }
        Log.d(TAG, "initTreinadores: hey");
        TreinadoresRecyclerViewAdapter adapter = new TreinadoresRecyclerViewAdapter(this, mTreinadores);
        adapter.notifyDataSetChanged();

        RecyclerView recyclerView = findViewById(R.id.RecyclerViewTreinadores);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Button novoTreinador = findViewById(R.id.botaoNovoTreinador);
        
        try{
            novoTreinador.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        handleAddTreinador();
                    } catch (Exception e) {
                        Log.d(TAG, "onClick: click exep: " + e);
                    }
                }
            });
        }catch (Exception e){
            Log.d(TAG, "initTreinadores: karien");
        }
    }

    protected void readFile() throws IOException {
        //read file - get treinadores' names "/data/user/0/com.example.taira.pokedex/files/treinadores.csv"

        //getDataDir() + "/files/treinadores.csv"

        BufferedReader reader = new BufferedReader(new FileReader(getDataDir() + "/files/treinadores.csv"));
        String[] fields = new String[0];
        ArrayList <String> list = new ArrayList<>();
        ArrayList<String> nameList = new ArrayList<>();

        while (true) {
            String line = reader.readLine();
            if (line == null)
                break;
            fields = line.split(",");
            for (int i=0; i<fields.length; i++){
                nameList.add(fields[i]);
                mTreinadores.add(new Treinador(fields[i]));
            }
        }
        for(int i=0; i<mTreinadores.size(); i++) {
            Log.d(TAG, "fakeFileReading: tteus: " + mTreinadores.get(i).getName());
        }

        reader.close();

    }
    
    protected void handleAddTreinador() throws IOException {

        Log.d(TAG, "handleAddTreinador: eita, clicou");
        try {
            startActivity(new Intent(this, MakeNewTreinador.class));
            Log.d(TAG, "handleAddTreinador: aqui estamos");
        }catch (Exception e){
            Log.d(TAG, "handleAddTreinador: nao, estamos aqui: " + e);
        }

    }
}