package com.example.taira.pokedex;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    private static final String LOLSTAT = "to_lol_stat";
    private static final String TAG = "EITA";
    private ArrayList<Pokemon> mPokemon = new ArrayList<>();
    private Context mContext;
    private String trainerName;
    private String task;


    private GestureDetector mDetector;

    ArrayList<Pokemon> selectedPokemon = new ArrayList<Pokemon>();

    public RecyclerViewAdapter(Context mContext, ArrayList<Pokemon> mPokemon, String task) {

        this.mContext = mContext;
        this.mPokemon = mPokemon;
        this.task = task;

    }
    public RecyclerViewAdapter(Context mContext, ArrayList<Pokemon> mPokemon, String task, String trainerName) {

        this.mContext = mContext;
        this.mPokemon = mPokemon;
        this.task = task;
        this.trainerName = trainerName;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        //Log.d(TAG, "onCreateViewHolder: called");

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pokemon, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);


        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
        //Log.d(TAG, "onBindViewHolder: called.");

        Glide.with(mContext)
                .asBitmap()
                .load(mPokemon.get(position).getAvatarLocation())
                .into(viewHolder.imageView);
        viewHolder.textView.setText(mPokemon.get(position).getName());

        viewHolder.parentLayout.setOnLongClickListener(new View.OnLongClickListener(){
            @Override
            public boolean onLongClick(View v) {
                Log.d(TAG, "onLongClick: long clicked on: " + mPokemon.get(position).getName());
                Log.d(TAG, "onLongClick: Layout position " + viewHolder.getLayoutPosition());
                Log.d(TAG, "onLongClick: poke numer " + mPokemon.get(position).getNumber());
                selectedPokemon.add(mPokemon.get(position));
                try {
                    if(task == "add") {
                        writeAddPokemon(mPokemon.get(position));
                        Toast.makeText(mContext, mPokemon.get(position).getName() + " adicionado", Toast.LENGTH_SHORT).show();

                    }else if(task == "remove"){
                        writeRemovePokemon(mPokemon.get(position));
                        Toast.makeText(mContext, mPokemon.get(position).getName() + " removido", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    Log.d(TAG, "onLongClick: exepat writinpoke" + e);
                }

                return true;
            }
        });
        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toStat = new Intent(mContext, StatsShow.class );
                toStat.putExtra(LOLSTAT, String.valueOf(mPokemon.get(position).getNumber()));
                mContext.startActivity(toStat);
            }
        });
    }

    public void writeAddPokemon(Pokemon pokemon) throws IOException {

        String pokeStorePath =  mContext.getDataDir() + "/files/" + trainerName + ".csv";
        Log.d(TAG, "writeAddPokemon: path: " + pokeStorePath);

        BufferedReader reader = new BufferedReader(new FileReader(pokeStorePath));
        String line = reader.readLine();
        Log.d(TAG, "writeAddPokemon: readi  ng from: " + pokeStorePath);
        Log.d(TAG, "writeAddPokemon: line: " + line);
        reader.close();
        BufferedWriter writer = new BufferedWriter(new FileWriter(pokeStorePath));

        writer.append(line + "," + pokemon.getNumber() );
        Log.d(TAG, "writeAddPokemon: writing pokenumer: " + pokemon.getNumber());
        writer.close();
    }

    public void writeRemovePokemon(Pokemon pokemon) throws IOException {
        String pokeStorePath =  mContext.getDataDir() + "/files/" + trainerName + ".csv";
        Log.d(TAG, "writeRemovePokemon: path: " + pokeStorePath);

        BufferedReader reader = new BufferedReader(new FileReader(pokeStorePath));
        String line = reader.readLine();
        Log.d(TAG, "writeRemovePokemon: line: " + line);
        String[] fields = line.split(",");
        reader.close();
        BufferedWriter writer = new BufferedWriter(new FileWriter(pokeStorePath));
        for (int i=0; i<fields.length; i++) {
            try{
                if (Integer.valueOf(fields[i]) != pokemon.getNumber()) {
                    writer.append("," + fields[i]);
                    Log.d(TAG, "writeRemovePokemon: append: " + fields[i]);
                }
            }catch(Exception e){

            }

        }
        //writer.append(line + "," + pokemon.getNumber() );
        //writer.write(",1,2,150,852");
        Log.d(TAG, "writeAddPokemon: writing pokenumer: " + pokemon.getNumber());
        writer.close();
        reader = new BufferedReader(new FileReader(pokeStorePath));
        line = reader.readLine();
        Log.d(TAG, "writeRemovePokemon: line: " + line);
        reader.close();
    }


    @Override
    public int getItemCount() {

        return mPokemon.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView textView;
        //CheckBox checkBox;
        RelativeLayout parentLayout;

        public ViewHolder( @NonNull View itemView) {

            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.textView);
            parentLayout = itemView.findViewById(R.id.parentLayout);
        }
    }

    public void filtraLista(ArrayList<Pokemon> filteredPokemon){
        Log.d(TAG, "filtraLista: fuck");
        this.mPokemon = filteredPokemon;
        notifyDataSetChanged();

    }

    public ArrayList<Pokemon> getSelectedPokemon() {
        return selectedPokemon;
    }

    public String getTrainerName() {
        return trainerName;
    }

    public void setTrainerName(String trainerName) {
        this.trainerName = trainerName;
    }
}
